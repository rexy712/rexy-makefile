#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#Copyright 2018-2021 rexy712

#Makefile to generate a single static or shared library from all the sources in SOURCE_DIRS ending in EXT

ifeq ($(OS),Windows_NT)
	WINDOWS::=1
endif

SOURCE_DIRS::=src
SOURCES::=
OBJDIR::=obj
DEPDIR::=$(OBJDIR)/dep
LIBDIRS::=lib
INCLUDE_DIRS::=include
PKGS::=
CFLAGS::=-std=c18 -Wall -pedantic -Wextra
CXXFLAGS::=-std=c++17 -Wall -pedantic -Wextra
DEBUG_CFLAGS::=$(CFLAGS) -O0 -g3 -ggdb
DEBUG_CXXFLAGS::=$(CXXFLAGS) -O0 -g3 -ggdb
RELEASE_CFLAGS::=$(CFLAGS) -O2 -Wno-strict-aliasing
RELEASE_CXXFLAGS::=$(CXXFLAGS) -O2 -Wno-strict-aliasing
EXT::=cpp
LANG::=$(EXT)
MAIN_LIBRARY::=tester
SHARED?=1
STATIC?=0
RELEASE?=0
MEMCHK?=0
UNDEFCHK?=0
SAVEFLAGS?=1

ifneq ($(WINDOWS),1)
	#*nix settings
	CC::=gcc
	CXX::=g++
	LDLIBS::=
	LDFLAGS::=
	DEBUG_LDLIBS::=
	DEBUG_LDFLAGS::=
	STRIP::=strip
	RANLIB::=ranlib
	AR::=ar
	AS::=as
	ifeq ($(SHARED),1)
		INTERNAL_SHARED_LIBRARY::=lib$(MAIN_LIBRARY).so
	endif
else #windows
	#windows settings
	#windows is a fuckwit
	MINGW_PREFIX::=x86_64-w64-mingw32-
	CC::=$(MINGW_PREFIX)gcc
	CXX::=$(MINGW_PREFIX)g++
	LDLIBS::=
	LDFLAGS::=-static-libgcc -static-libstdc++
	DEBUG_LDLIBS::=
	DEBUG_LDFLAGS::=-static-libgcc -static-libstdc++
	STRIP::=$(MINGW_PREFIX)strip
	RANLIB::=$(MINGW_PREFIX)ranlib
	AR::=$(MINGW_PREFIX)ar
	AS::=$(MINGW_PREFIX)as
	ifeq ($(SHARED),1)
		INTERNAL_SHARED_LIBRARY::=$(MAIN_LIBRARY).dll
	endif
endif #windows
ifeq ($(STATIC),1)
	INTERNAL_STATIC_LIBRARY::=lib$(MAIN_LIBRARY).a
endif

#Put your custom targets for PRE_TARGETS and POST_TARGETS here:

#default target
.PHONY: all

#pre targets
all::

all:: $(INTERNAL_STATIC_LIBRARY) $(INTERNAL_SHARED_LIBRARY)

#post targets
all::

#custom clean targets
clean::

###########################################################################################################
#Everything past this point is internal BS, probably best not to touch it unless you know what you're doing

#set the all target as the default target, otherwise the topmost target will run
.DEFAULT_GOAL::=all

#setup the actual output library name depending on shared/static and windows/anything else
#system dependant bullshit
ifeq ($(OS),Windows_NT)
	#windows' cmd commands
	mkdir=mkdir $(subst /,\,$(1)) > NUL 2>&1
	rm=del /F $(1) > NUL 2>&1
	rmdir=rd /S /Q $(1) > NUL 2>&1
	move=move /Y $(subst /,\,$(1)) $(subst /,\,$(2)) > NUL 2>&1
	copy=copy /Y /B $(subst /,\,$(1)) $(subst /,\,$(2)) > NUL 2>&1
else
	#*nix terminal commands
	mkdir=mkdir -p $(1)
	rm=rm -f $(1)
	rmdir=rm -rf $(1)
	move=mv $(1) $(2)
	copy=cp $(1) $(2)
endif

#setup compiler and flags based on language
ifeq ($(LANG),cpp)
	ifneq ($(RELEASE),1)
		COMPILER_FLAGS::=$(DEBUG_CXXFLAGS)
	else
		COMPILER_FLAGS::=$(RELEASE_CXXFLAGS)
	endif
	COMPILER::=$(CXX)
else ifeq ($(LANG),c)
	ifneq ($(RELEASE),1)
		COMPILER_FLAGS::=$(DEBUG_CFLAGS)
	else
		COMPILER_FLAGS::=$(RELEASE_CFLAGS)
	endif
	COMPILER::=$(CC)
endif

ifneq ($(RELEASE),1)
	ifeq ($(MEMCHK),1)
		#use asan to check memory leaks/invalid accesses
		LDFLAGS+=-fsanitize=address -fno-omit-frame-pointer -fno-optimize-sibling-calls
		COMPILER_FLAGS+=-fsanitize=address -fno-omit-frame-pointer -fno-optimize-sibling-calls -DENABLE_MEMCHK=1
	endif
	ifeq ($(UNDEFCHK),1)
		LDFLAGS+=-fsanitize=undefined
		COMPILER_FLAGS+=-fsanitize=undefined -DENABLE_UNDEFCHK=1
	endif
endif

ifeq ($(SAVEFLAGS),1)
	CFLAGS_TMPFILE::=.cflags.tmp
	LDFLAGS_TMPFILE::=.ldflags.tmp
	OLD_COMPILEFLAGS::=$(file <$(CFLAGS_TMPFILE))
	OLD_LINKFLAGS::=$(file <$(LDFLAGS_TMPFILE))
endif

ifeq ($(SHARED),1)
	COMPILER_FLAGS+=-DENABLE_SHARED=1
endif
ifeq ($(STATIC),1)
	COMPILER_FLAGS+=-DENABLE_STATIC=1
endif

#add dependency tracking and include directories
INTERNAL_COMPILERFLAGS=-c $(foreach dir,$(INCLUDE_DIRS),-I"$(dir)") $(foreach pkg,$(PKGS),$(shell pkg-config --cflags "$(pkg)")) -MMD -MP -MF"$(DEPDIR)/$(notdir $(patsubst %.o,%.d,$@))"
ifeq ($(WINDOWS),1)
INTERNAL_LINKFLAGS=-Wl,--out-implib,"lib$(MAIN_LIBRARY).a" $(foreach dir,$(LIBDIRS),-L"$(dir)") $(foreach pkg,$(PKGS),$(shell pkg-config --libs "$(pkg)"))
else
INTERNAL_LINKFLAGS=$(foreach dir,$(LIBDIRS),-L"$(dir)") $(foreach pkg,$(PKGS),$(shell pkg-config --libs "$(pkg)"))
endif
INTERNAL_SOURCES::=$(SOURCES) $(foreach source,$(SOURCE_DIRS),$(foreach ext,$(EXT),$(wildcard $(source)/*.$(ext))))
STATIC_OBJECTS::=$(addprefix $(OBJDIR)/static/,$(subst \,.,$(subst /,.,$(addsuffix .o,$(INTERNAL_SOURCES)))))
SHARED_OBJECTS::=$(addprefix $(OBJDIR)/shared/,$(subst \,.,$(subst /,.,$(addsuffix .o,$(INTERNAL_SOURCES)))))
ALL_COMPILEFLAGS=$(COMPILER_FLAGS) $(INTERNAL_COMPILERFLAGS)
ifeq ($(RELEASE),1)
	ALL_LINKFLAGS=$(INTERNAL_LINKFLAGS) $(LDFLAGS)
	ALL_LDLIBS=$(LDLIBS)
	ALL_LINK_FLAGS_LIBS=$(ALL_LINKFLAGS) $(ALL_LDLIBS)
else
	ALL_LINKFLAGS=$(INTERNAL_LINKFLAGS) $(LDFLAGS) $(DEBUG_LDFLAGS)
	ALL_LDLIBS=$(LDLIBS) $(DEBUG_LDLIBS)
	ALL_LINK_FLAGS_LIBS=$(ALL_LINKFLAGS) $(ALL_LDLIBS)
endif

ifeq ($(SAVEFLAGS),1)
ifneq ($(subst -MF"$(DEPDIR)/",-MF"$(DEPDIR)/$(CFLAGS_TMPFILE)",$(ALL_COMPILEFLAGS)),$(OLD_COMPILEFLAGS))
.PHONY: $(CFLAGS_TMPFILE)
$(CFLAGS_TMPFILE):
	$(file >$(CFLAGS_TMPFILE),$(ALL_COMPILEFLAGS))
else
$(CFLAGS_TMPFILE):
endif

ifneq ($(ALL_LINK_FLAGS_LIBS),$(OLD_LINKFLAGS))
.PHONY: $(LDFLAGS_TMPFILE)
$(LDFLAGS_TMPFILE):
	$(file >$(LDFLAGS_TMPFILE),$(ALL_LINK_FLAGS_LIBS))
else
$(LDFLAGS_TMPFILE):
endif
endif


#target for shared library
$(INTERNAL_SHARED_LIBRARY): $(SHARED_OBJECTS) $(LDFLAGS_TMPFILE)
	$(COMPILER) -shared -o "$@" $(SHARED_OBJECTS) -fPIC $(ALL_LINKFLAGS) $(ALL_LDLIBS)
ifeq ($(RELEASE),1)
	$(STRIP) --strip-debug "$@"
endif

#target for static library
$(INTERNAL_STATIC_LIBRARY): $(STATIC_OBJECTS)
	$(AR) rcs "$@" $^
	$(RANLIB) "$@"


#Object target recipe
define GENERATE_SHARED_OBJECTS
$$(OBJDIR)/shared/$(subst \,.,$(subst /,.,$(1))).o: $(1) $(CFLAGS_TMPFILE)
	$$(COMPILER) $$(ALL_COMPILEFLAGS) -fPIC "$$<" -o "$$@"
endef
#Object target recipe
define GENERATE_STATIC_OBJECTS
$$(OBJDIR)/static/$(subst \,.,$(subst /,.,$(1))).o: $(1) $(CFLAGS_TMPFILE)
	$$(COMPILER) $$(ALL_COMPILEFLAGS) "$$<" -o "$$@"
endef


#Create targets for object files
ifeq ($(SHARED),1)
$(foreach src,$(INTERNAL_SOURCES),$(eval $(call GENERATE_SHARED_OBJECTS,$(src))))
endif
ifeq ($(STATIC),1)
$(foreach src,$(INTERNAL_SOURCES),$(eval $(call GENERATE_STATIC_OBJECTS,$(src))))
endif

$(STATIC_OBJECTS): | $(OBJDIR)/static $(DEPDIR)
$(SHARED_OBJECTS): | $(OBJDIR)/shared $(DEPDIR)

$(OBJDIR):
	$(call mkdir,"$@")
$(OBJDIR)/static: $(OBJDIR)
	$(call mkdir,"$@")
$(OBJDIR)/shared: $(OBJDIR)
	$(call mkdir,"$@")
$(DEPDIR):
	$(call mkdir,"$@")

.PHONY: clean
clean::
	$(call rmdir,"$(OBJDIR)")
	$(call rmdir,"$(DEPDIR)")
	$(call rm,"lib$(MAIN_LIBRARY).so")
	$(call rm,"lib$(MAIN_LIBRARY).a")
	$(call rm,"$(MAIN_LIBRARY).dll")
ifeq ($(SAVEFLAGS),1)
	$(call rm,"$(CFLAGS_TMPFILE)")
	$(call rm,"$(LDFLAGS_TMPFILE)")
endif

#header file dep tracking
-include $(wildcard $(DEPDIR)/*.d)

